# Package

version       = "0.0.1"
author        = "Dillen Meijboom"
description   = "-"
license       = "MIT"
srcDir        = "src"
bin           = @["precommit"]


# Dependencies

requires "nim >= 1.4.2"
requires "yaml#head"
requires "cligen >= 1.4.0"
