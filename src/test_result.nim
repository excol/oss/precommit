type
  TestResult* = tuple[output: string, exitCode: int]
