import osproc
import strutils

type
  GitStatusEntry* = ref object of RootObj
    staged*: bool
    filename*: string
    objectName*: string
    fileMode*: string

  GitStatus* = ref object of RootObj
    entries*: seq[GitStatusEntry]
    untracked*: seq[string]

proc parseHeader(line: string): GitStatusEntry =
  # git calls this changed X,Y not sure what this means
  let changed = line.substr(2, 4)
  # submodule state ("N..." means that it's not part of a submodule)
  let submodule = line.substr(5, 9)
  let fileModeHead = line.substr(10, 16)
  let fileModeIndex = line.substr(17, 23)
  let fileModeWorkTree = line.substr(24, 30)
  let objectNameHead = line.substr(31, 71)
  let objectNameIndex = line.substr(72, 112)

  return GitStatusEntry(
    staged: changed[0] != '.',
    fileMode: fileModeIndex,
    objectName: objectNameIndex
  )

proc exec(args: openArray[string]): string =
  let cmd = execCmdEx("git $#" % [args.join(" ")])

  if cmd.exitCode != 0:
    raise newException(OSError, "invalid exit code: $#" % [$ cmd.exitCode])

  return cmd.output

proc gitStatus*(): GitStatus =
  let status = GitStatus()
  let output = exec(["status", "--porcelain=2"])

  for line in splitLines(output):
    if line.len == 0:
      continue

    case line[0]
    of '?':
      status.untracked.add(line.substr(2))
    of '1':
      let entry = parseHeader(line)

      entry.filename = line.substr(113)

      status.entries.add(entry)
    else:
      continue

  return status

proc getObject*(hash: string): string =
  exec(["cat-file", "-p", hash])
