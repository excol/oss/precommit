import json
import osproc
import options
import strutils
import terminal

import linter
import test_result

type
  Stage = enum
    stCommit

  TestMode = enum
    tmNormal = "normal",
    tmWarn = "warn"

  Test = ref object of RootObj
    name: string
    cmd: string
    mode: Option[TestMode]

  Config = ref object of RootObj
    commit: seq[Test]

proc runTest(test: Test): TestResult =
  if test.cmd.startsWith("run://"):
    let name = test.cmd.substr(6)

    case name
    of "lint": runLinter()
    else: (
      "unknown action $#" % [name],
      1
    )
  else:
    execCmdEx(
      test.cmd,
      options={poStdErrToStdOut}
    )

proc runStage(config: Config, stage: Stage): bool =
  let tests = case stage
    of stCommit: config.commit
  let name = case stage
    of stCommit: "commit"

  var success = true

  stdout.styledWrite(fgBlue, "($#)\n\n" % [name])

  for test in tests:
    if not success:
      echo " $#" % [test.name]
      continue

    echo ".. $#" % [test.name]

    stdout.cursorUp()
    stdout.eraseLine()

    let cmd = runTest(test)

    if cmd.exitCode == 0:
      echo " $#" % [test.name]
    else:
      let mode = if test.mode.isSome():
        test.mode.get()
      else:
        tmNormal
      let color = case mode
        of tmWarn: fgYellow
        else:
          fgRed
      let icon = case mode
        of tmWarn: ""
        else: ""

      if mode != tmWarn:
        success = false

      echo "$# $# (exit with status: $#)" % [icon, test.name, $ cmd.exitCode]

      stderr.styledWrite(color, cmd.output)

      if not cmd.output.endsWith("\n"):
        stderr.write("\n")

  return success

proc commit(wait: bool=false) =
  let node = parseFile(".pre-commit.json")
  let config = node.to(Config)

  if not runStage(config, stCommit):
    if wait:
      while true:
        discard stdin.readLine()

        stdout.write("\ec")
        stdout.flushFile()

        if runStage(config, stCommit):
          return
        else:
          continue

    quit(1)

when isMainModule:
  import cligen

  dispatchMulti(
    [
      commit,
    ]
  )
