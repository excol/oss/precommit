import strutils

from os import splitFile
from streams import newStringStream
from json import parseJson, JsonParsingError
from osproc import execCmdEx, poStdErrToStdOut

import yaml

import git
import test_result

proc lintGo(entry: GitStatusEntry): TestResult =
  let content = getObject(entry.objectName)
  let (output, exitCode) = execCmdEx(
    "gofumpt -s -e -d",
    { poStdErrToStdOut },
    input=content
  )

  if exitCode == 0:
    if output.len > 0:
      return (output.replace("<standard input>", entry.filename), 1)

  (output, exitCode)

proc lintJSON(entry: GitStatusEntry): TestResult =
  try:
    let content = getObject(entry.objectName)

    discard parseJson(
      newStringStream(content),
      entry.filename,
      false,
      false
    )
  except JsonParsingError as e:
    return (e.msg, 1)

  return ("", 0)

proc lintYAML(entry: GitStatusEntry): TestResult =
  let content = getObject(entry.objectName)
  let parser = initYamlParser()

  try:
    var stream = parser.parse(content)
    discard constructJson(stream)
  except YamlStreamError as e:
    let exc = cast[ref YamlParserError](e.parent)
    let (_, filename, ext) = splitFile(entry.filename)

    return ("$#$#($#): $#" % [
      filename,
      ext,
      $ exc.mark.line,
      exc.msg
    ], 1)

  return ("", 0)

proc runLinter*(): TestResult =
  let status = gitStatus()

  for entry in status.entries:
    if not entry.staged:
      continue

    let (_, _, ext) = splitFile(entry.filename)
    let testResult = case ext
      of ".json": lintJSON(entry)
      of ".yaml": lintYAML(entry)
      of ".yml": lintYAML(entry)
      of ".go": lintGo(entry)
      else:
        continue

    if testResult.exitCode != 0:
      return testResult

  return ("", 0)
